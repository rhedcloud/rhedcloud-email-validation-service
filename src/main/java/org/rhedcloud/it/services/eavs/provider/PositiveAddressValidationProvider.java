package org.rhedcloud.it.services.eavs.provider;

import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

public class PositiveAddressValidationProvider extends OpenEaiObject implements EmailAddressValidationProvider {
	
	AppConfig m_appConfig;
    @Override
    public void init(AppConfig aConfig) throws ProviderException {
    	m_appConfig = aConfig;
    }
    

    @Override
    public EmailAddressValidation query(EmailAddressValidationQuerySpecification qs) throws ProviderException {
    	try {
			EmailAddressValidation eav = (EmailAddressValidation) m_appConfig.getObject("EmailAddressValidation.v1_0");
			eav.setValidationProvider("PositiveEmailAddressValidationProvider");
			eav.setStatusCode("0");
			eav.setStatusDescription("You are always valid - build 352");
			eav.setEmailAddress(qs.getEmailAddress());
			return eav;
			
		} catch (EnterpriseConfigurationObjectException e) {
			throw new ProviderException("EnterpriseConfigurationObjectException",e);
		} catch (EnterpriseFieldException e) {
			throw new ProviderException("EnterpriseConfigurationObjectException",e);
		}
    }
}
