package org.rhedcloud.it.services.eavs.provider;

import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;

public class ExampleEmailAddressValidationProvider extends OpenEaiObject implements EmailAddressValidationProvider {
    @Override
    public void init(AppConfig aConfig) throws ProviderException {
    }

    @Override
    public EmailAddressValidation query(EmailAddressValidationQuerySpecification qs) throws ProviderException {
        return null;
    }
}
