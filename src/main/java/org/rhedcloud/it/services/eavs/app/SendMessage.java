package org.rhedcloud.it.services.eavs.app;

import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class SendMessage implements MessageListener{

	private static ActiveMQConnectionFactory mConnectionFactory;
	
	private MessageConsumer mConsumer;
	
	
	
	public SendMessage(MessageConsumer mConsumer) {
		//super();
		this.mConsumer = mConsumer;
	}

	private static String queryMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<!DOCTYPE edu.emory.Validation.EmailAddressValidation.Query SYSTEM \"../dtd/Query-Request.dtd\">"
			+ "<edu.emory.Validation.EmailAddressValidation.Query>"
			+ "<ControlAreaRequest messageCategory=\"edu.emory.Validation\" messageObject=\"EmailAddressValidation\" messageAction=\"Query\" messageRelease=\"1.0\" messagePriority=\"9\" messageType=\"Request\">\n"
			+ "<Sender><MessageId><SenderAppId>org.rhedcloud.EmailAddressValidationService</SenderAppId><ProducerId>d21b9d6f-5f95-48f4-b40d-e42ab44814bf</ProducerId><MessageSeq>1</MessageSeq></MessageId><Authentication><AuthUserId>not used</AuthUserId><AuthUserSignature>not used</AuthUserSignature></Authentication></Sender><Datetime><Year>2021</Year><Month>6</Month><Day>9</Day><Hour>6</Hour><Minute>54</Minute><Second>38</Second><SubSecond>70</SubSecond><Timezone>America/Chicago</Timezone></Datetime><ExpectedReplyFormat messageCategory=\"edu.emory.Validation\" messageObject=\"EmailAddressValidation\" messageAction=\"Provide\" messageRelease=\"1.0\" messageType=\"Reply\" /></ControlAreaRequest>\n"
			+ "<DataArea>\n"
			+ "<EmailAddressValidationQuerySpecification><EmailAddress>tom@cervenka.org</EmailAddress><ValidationProvider>PositiveEmailAddressValidationProvider</ValidationProvider></EmailAddressValidationQuerySpecification></DataArea>\n"
			+ "</edu.emory.Validation.EmailAddressValidation.Query>\n"
			;			

	static boolean allConsumersCameAndWent(Vector<SendMessage> listeners) {
		for(SendMessage listener:listeners) {
			//System.out.println("allConsumersCameAndWent: listener.isConsumerDone() == "+listener.isConsumerDone());
			if (listener.isConsumerDone()==false) {
				return false;
			}
		}
		return true;
	};

	public static void main(String[] args) {

//		Lambda broker
//		mConnectionFactory = new ActiveMQConnectionFactory("failover:(ssl://b-b4227e12-03f6-497b-a8d9-dbe8889c2569-1.mq.us-east-1.amazonaws.com:61617,ssl://b-b4227e12-03f6-497b-a8d9-dbe8889c2569-2.mq.us-east-1.amazonaws.com:61617)");
//		mConnectionFactory.setPassword("29n845-8PXEB");
//		mConnectionFactory.setUserName("Eavs-lambda");
//		EMORY-NONPROD
		mConnectionFactory = new ActiveMQConnectionFactory("failover:(nio+ssl://b-e6707dbc-f49e-4bad-afcf-b736314bd2d9-1.mq.us-east-1.amazonaws.com:61617,nio+ssl://b-e6707dbc-f49e-4bad-afcf-b736314bd2d9-2.mq.us-east-1.amazonaws.com:61617)?maxReconnectAttempts=0");
//		mConnectionFactory.setPassword("nH44lIuhRVZfj5XTeM1lzw");
//		mConnectionFactory.setUserName("EmailAddressValidationService");
		mConnectionFactory.setPassword("nH44lIuhRVZfj5XTeM1lzw2");
		mConnectionFactory.setUserName("Eavs");
		Connection mConnection;
		try {
			mConnection = mConnectionFactory.createConnection();
		} catch (JMSException e) {
			e.printStackTrace();
			return;
		}
		System.out.println("Connection created");
		System.out.println("mConnection = "+mConnection);
		Session session = null;
		Destination destination = null;
		MessageProducer producer = null;
		MessageConsumer consumer = null;
		Vector<SendMessage> listeners = new Vector<SendMessage>();
		try {
			mConnection.start();
			session = mConnection.createSession(false, 1);
			System.out.println("Session created");
			System.out.println("session = "+session);
//			destination = session.createQueue("EaVs-public");
			destination = session.createQueue("EmailAddressValidationServiceQueue_DEV");
			System.out.println("Destination created");
			System.out.println("destination = "+destination);
			producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			System.out.println("Producer created");
			System.out.println("producer = "+producer);
//			TemporaryQueue tempQueue = session.createTemporaryQueue();
			Date start = new Date();
			for (int i=0;i<1;i++) {
				TemporaryQueue tempQueue = session.createTemporaryQueue();
				consumer = session.createConsumer(tempQueue);
				SendMessage listener = new SendMessage(consumer);
				listeners.add(listener);
				consumer.setMessageListener(listener);

				
				Message message = session.createTextMessage(queryMessage);
				message.setJMSReplyTo(tempQueue);
				message.setJMSTimestamp(new Date().getTime());
				message.setJMSDeliveryMode(2);
				String correlationID = "correlationID."+i+"."+message.getJMSTimestamp();
				message.setJMSCorrelationID(correlationID);
				message.setJMSRedelivered(false);
				//type
				message.setJMSExpiration(0);
				message.setJMSPriority(9);
	
				listener.sendMessage(producer,message);
				
			}

			Date waitStart = new Date();
			System.out.println("Waiting for all consumers to stop");
			final long timeoutMs = 60000;
			while (new Date().getTime()-waitStart.getTime()<timeoutMs 
					&& allConsumersCameAndWent(listeners)==false ) {
				TimeUnit.MILLISECONDS.sleep(10);
			}
			if (!allConsumersCameAndWent(listeners)) {
				System.out.println("WARNING: Timeout waiting for consumer count to zero out");
			}
			long accumulatedTime = 0;
			long max = 0, min = 999999999;
			for(SendMessage listner: listeners) {
				long rt = listner.getTime();
				accumulatedTime += rt;
				max = rt > max ? rt : max ;
				min = rt < min ? rt : min ;
			}
			System.out.println("Elapsed time in ms: "+(new Date().getTime()-start.getTime()));
			System.out.println("Number of requests = "+listeners.size());
			System.out.println("Total time waiting for requests = "+accumulatedTime);
			long accumulatedTimeAvg = (accumulatedTime/listeners.size());
			System.out.println("Average time waiting for a request (simple) = "+accumulatedTimeAvg
					+" +"+(max-accumulatedTimeAvg)
					+" -"+(accumulatedTimeAvg-min)
					);
			System.out.println("Max time waiting for a request = "+max);
			System.out.println("Min time waiting for a request = "+min);
			
		} catch (JMSException e) {
			e.printStackTrace();
			return;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			producer.close();
			session.close();
			mConnection.close();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	private long getTime() {
		if (!isConsumerDone() ) {
			return -1; 
		}
		return responseReceivedTime - requestSentTime;
	}

	private void sendMessage(MessageProducer producer, Message message) {
		new ProducerThread(producer,message,this).start();
	}

	long requestSentTime;
	long responseReceivedTime;

	public SendMessage() {
		super();
	}

	private boolean consumerDone=false;

	public Message message;
	
	public boolean isConsumerDone() {
		return consumerDone;
	}
		

	@Override
	public void onMessage(Message message) {
		try {
			this.consumerDone = true;
			this.responseReceivedTime = new Date().getTime();
			mConsumer.close();
			System.out.println("Received message: "+message.getJMSCorrelationID()+" response time in ms is "+(responseReceivedTime-requestSentTime));
			System.out.println("Data: "+((TextMessage) message).getText());
		} catch (JMSException e) {
			e.printStackTrace();
		}
		
	}
	
	public class ProducerThread extends Thread {
		MessageProducer producer;
		Message message;
		SendMessage parent;
		
		public ProducerThread(MessageProducer producer, Message message, SendMessage sm) {
			super();
			this.producer = producer;
			this.message = message;
			this.parent = sm;
		}

		@Override
		public void run() {
			super.run();
			try {
				producer.send(this.message);
				parent.requestSentTime=new Date().getTime();
				System.out.println("Sent message: "+ message.getJMSCorrelationID() + " : " + Thread.currentThread().getName());
			} catch (JMSException e) {
				System.err.println("error sending message: "+e.getMessage());
			}
		}
		
	}
	


}
