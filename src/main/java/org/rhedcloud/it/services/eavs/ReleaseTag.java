package org.rhedcloud.it.services.eavs;

/**
 * A release tag for the Emory Email Address Validation Service.
 * <p>
 *
 * @author Kevin Hale Boyes (khaleboyes@surgeforward.com)
 * @version 1.0  - 22 January 2018
 */
public abstract class ReleaseTag {
    public static String space = " ";
    public static String notice = "***";
    public static String releaseName = "RHEDcloud Email Address Validation Service";
    public static String releaseNumber = "Release 1.0";
    public static String buildNumber = "Build ####";
    public static String copyRight = "Copyright 2018 Emory University. All Rights Reserved.";

    public static String getReleaseInfo() {
        StringBuilder buf = new StringBuilder();
        buf.append(notice);
        buf.append(space);
        buf.append(releaseName);
        buf.append(space);
        buf.append(releaseNumber);
        buf.append(space);
        buf.append(buildNumber);
        buf.append(space);
        buf.append(copyRight);
        buf.append(space);
        buf.append(notice);
        return buf.toString();
    }
}
