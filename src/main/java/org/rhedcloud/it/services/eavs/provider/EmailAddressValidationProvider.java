package org.rhedcloud.it.services.eavs.provider;

import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;
import org.openeai.config.AppConfig;

public interface EmailAddressValidationProvider {
    /**
     * Initialize the provider.
     * <P>
     * 
     * @param aConfig,
     *            an AppConfig object with all this provider needs.
     *            <P>
     * @throws ProviderException
     *             with details of the initialization error.
     */
    void init(AppConfig aConfig) throws ProviderException;

    /**
     * Query for the validity of an email address.
     * <P>
     * 
     * @param qs
     *            the query spec.
     * @return EmailAddressValidation, the validation status.
     *         <P>
     * @throws ProviderException
     *             with details of the providing the list.
     */
    EmailAddressValidation query(EmailAddressValidationQuerySpecification qs) throws ProviderException;
}
