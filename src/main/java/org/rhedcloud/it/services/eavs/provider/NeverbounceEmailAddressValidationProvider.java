package org.rhedcloud.it.services.eavs.provider;

import com.neverbounce.api.client.NeverbounceClient;
import com.neverbounce.api.client.NeverbounceClientFactory;
import com.neverbounce.api.client.exception.NeverbounceApiException;
import com.neverbounce.api.client.exception.NeverbounceIoException;
import com.neverbounce.api.model.SingleCheckResponse;
import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;

/**
 * Validate email addresses using NeverBounce. See
 * https://developers.neverbounce.com/v4.0/reference and
 * https://neverbounce.github.io/NeverBounceApi-Java and
 * https://neverbounce.com/help/understanding-and-downloading-results/result-codes
 */
public class NeverbounceEmailAddressValidationProvider extends OpenEaiObject implements EmailAddressValidationProvider {

    private static final String LOGTAG = "[NeverbounceEmailAddressValidationProvider] ";
    private static final Logger logger = OpenEaiObject.logger;

    private static final String NEVERBOUNCE_PROVIDER_PROPERTIES = "NeverbounceEmailAddressValidationProvider";

    // eavsProviderProperties PropertyConfig keys
    private static final String PROP_API_KEY = "apiKey";

    private AppConfig appConfig;

    // NeverBounce client is thread safe
    private NeverbounceClient neverbounceClient;

    /*
     * default values for properties are specified here, but can be overridden
     * at runtime via corresponding properties from config doc
     */
    /** NeverBounce API key */
    private String apiKey = "CredentialRedactedAndRotated"; // PROP_API_KEY

    @Override
    public void init(AppConfig aConfig) throws ProviderException {
        logger.info(LOGTAG + " - Initializing");
        setAppConfig(aConfig);
        setProviderProperties();
        createNeverBounceClient();
    }

    @Override
    public EmailAddressValidation query(EmailAddressValidationQuerySpecification qs) throws ProviderException {
        performPreActionSetup();

        String emailAddress = qs.getEmailAddress();

        try {
            SingleCheckResponse nbr;
            long startTiming = System.currentTimeMillis();
            try {
                nbr = neverbounceClient.prepareSingleCheckRequest().withEmail(emailAddress).withAddressInfo(true)
                        .withCreditsInfo(true).withTimeout(300).build().execute();
            } finally {
                long duration = System.currentTimeMillis() - startTiming;
                logger.info(LOGTAG + " - NeverBounce single-check request complete in " + duration + " ms.");
            }

            EmailAddressValidation eav = (EmailAddressValidation) getAppConfig().getObject("EmailAddressValidation.v1_0");

            // Set the current layout manager to be the
            // NeverbounceEmailValidationXmlLayout layout manager.
            eav.setInputLayoutManager((EnterpriseLayoutManager) eav.getInputLayoutManagers().get("other"));

            /*
             * if the response from Neverbounce gives an INVALID result then the
             * address info can sometimes be blank. but an email address is
             * required in the EmailAddressValidation response so arrange for
             * the query email to be available to the layout code.
             */
            if (nbr.getAddressInfo().getOriginalEmail() == null || nbr.getAddressInfo().getOriginalEmail().isEmpty()) {
                nbr.getAddressInfo().setOriginalEmail(emailAddress);
            }

            eav.buildObjectFromInput(nbr);

            // Set the layout manager back to the XML layout manager.
            eav.setInputLayoutManager((EnterpriseLayoutManager) eav.getInputLayoutManagers().get("xml"));

            return eav;

        } catch (NeverbounceApiException | NeverbounceIoException e) {
            String errMsg = "Error retrieving response from NeverBounce: The exception is: " + e.getMessage();
            logger.error(LOGTAG + " - " + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error generating EmailAddressValidation.v1_0 object.  The exception is: " + e.getMessage();
            logger.error(LOGTAG + " - " + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (EnterpriseLayoutException e) {
            String errMsg = "Error laying out EmailAddressValidation object.  The exception is: " + e.getMessage();
            logger.error(LOGTAG + " - " + errMsg, e);
            throw new ProviderException(errMsg, e);
        } catch (Exception e) {
            String errMsg = "Unknown error.  The exception is: " + e.getMessage();
            logger.error(LOGTAG + " - " + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    private void performPreActionSetup() throws ProviderException {
        // refresh local variables
        logger.info(LOGTAG + " - Refreshing local variables ...");
        refreshLocalVariables();
        logger.info(LOGTAG + " - Finished refreshing local variables");
    }

    /**
     * Reset all relevant local variables to account for any config doc changes
     * that may have occurred since last time provider was used.
     *
     * @throws ProviderException
     *             on error
     */
    private void refreshLocalVariables() throws ProviderException {
        // do this here so config doc changes will be picked up without a
        // service bounce
        setProviderProperties();

        // if the NeverBounce API key changes then create a new client
        String previousApiKey = apiKey;
        apiKey = getProperties().getProperty(PROP_API_KEY, apiKey);
        if (!apiKey.equals(previousApiKey))
            createNeverBounceClient();
    }

    private void setProviderProperties() throws ProviderException {
        try {
            setProperties(getAppConfig().getProperties(NEVERBOUNCE_PROVIDER_PROPERTIES));
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving the " + NEVERBOUNCE_PROVIDER_PROPERTIES
                    + " PropertyConfig object from AppConfig: The exception is: " + e.getMessage();
            logger.error(LOGTAG + " - " + errMsg, e);
            throw new ProviderException(errMsg, e);
        }
    }

    private void createNeverBounceClient() {
        neverbounceClient = NeverbounceClientFactory.create(apiKey);
    }

    private AppConfig getAppConfig() {
        return appConfig;
    }
    private void setAppConfig(AppConfig v) {
        appConfig = v;
    }
}
