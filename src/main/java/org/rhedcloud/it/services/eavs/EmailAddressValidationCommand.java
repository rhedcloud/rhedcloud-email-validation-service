package org.rhedcloud.it.services.eavs;

import com.openii.openeai.commands.MessageMetaData;
import org.apache.logging.log4j.Logger;
import org.rhedcloud.it.services.eavs.provider.EmailAddressValidationProvider;
import org.rhedcloud.it.services.eavs.provider.ProviderException;
import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that handles Query requests for the Emory Email Address Validation
 * Service.
 * <p>
 *
 * @author Kevin Hale Boyes (khaleboyes@surgeforward.com)
 * @version 1.0 - 22 January 2018
 */
public class EmailAddressValidationCommand extends EavsRequestCommand implements RequestCommand {
    Logger LOG= org.apache.logging.log4j.LogManager.getLogger(EmailAddressValidationCommand.class);
    private static final String LOGTAG = "[EmailAddressValidationCommand] ";
    private static final String REQUIRED_MESSAGE_OBJECT = "EmailAddressValidation";
    private static final String EAVS_PROVIDER_CLASS_NAME = "eavsProviderClassName";

    private Document _genericResponseReplyDoc;

    private EmailAddressValidationProvider provider = null;

    public EmailAddressValidationCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        info("Initializing " + ReleaseTag.getReleaseInfo());

        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            _genericResponseReplyDoc = xmlReader.initializeDocument(getProperties().getProperty("GenericResponseDocumentUri"),
                    getInboundXmlValidation());
        } catch (XmlDocumentReaderException e) {
            e.printStackTrace();
            throw new InstantiationException(e.getMessage());
        }
        if (_genericResponseReplyDoc == null) {
            throw new InstantiationException("Missing 'GenericResponseDocumentUri' property in configuration document.  Can't continue.");
        }

        // Initialize an EmailAddressValidationProvider
        String providerClassName = getProperties().getProperty(EAVS_PROVIDER_CLASS_NAME);
        if (providerClassName == null || providerClassName.equals("")) {
            String errMsg = "No " + EAVS_PROVIDER_CLASS_NAME + " property specified. Can't continue.";
            logger.fatal(LOGTAG + " - " + errMsg);
            throw new InstantiationException(errMsg);
        }

        // set the initial provider, this may change when/if the configuration
        // is changed during execute
        setProvider(initializeProvider(providerClassName));
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {

    	// Get the execution start time.
        long startTime = System.currentTimeMillis();

        info("[execute] - begins ...");
        String errMessage;

        MessageMetaData mmd;
        try {
            mmd = initializeMetaData(messageNumber, aMessage);
        } catch (Exception e) {
            errMessage = "Exception occurred processing input message in ConsumerCommand.  Exception: " + e.getMessage();
            logExecutionComplete("Unknown", startTime);
            throw new CommandException(errMessage, e);
        }

        info("[execute] - Processing a: " + mmd.getMessageCategory() + "." + mmd.getMessageObject() + "." + mmd.getMessageAction() + "-"
                + mmd.getMessageType());

        // retrieve text portion of message passed in
        TextMessage msg = (TextMessage) aMessage;
        try {
            msg.clearBody(); // So we don't have to do it later...
        } catch (JMSException e) {
            logExecutionComplete(mmd.getMessageAction(), startTime);
            throw new CommandException(e.getMessage(), e);
        }

        Document replyDoc = (Document) getReplyDoc(mmd.getMessageAction(), mmd.getMessageObject(), mmd.getMessageRelease()).clone();

        // TODO: check to see if providerClassName has changed due to a config
        // change
        // if it has, need to reinitialize the provider implementation.

        // Verify that the message object we are dealing with is what we expect.
        // If not, reply with an error.
        if (!mmd.getMessageObject().equalsIgnoreCase(REQUIRED_MESSAGE_OBJECT)) {
            Document genericDoc = (Document) _genericResponseReplyDoc.clone();

            String errType = "application";
            String errCode = "OpenEAI-1001";
            String errDesc = "Unsupported message object: " + mmd.getMessageObject() + ". " + LOGTAG + " expects '"
                    + REQUIRED_MESSAGE_OBJECT + "'.";
            logger.fatal(LOGTAG + " - " + errDesc);
            logger.fatal("Message sent in is:\n" + getMessageBody(mmd.getInDoc()));
            List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            String replyContents = buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors);
            logExecutionComplete(mmd.getMessageAction(), startTime);
            return getMessage(msg, replyContents);
        }

        // check the incoming action and take appropriate steps
        if (mmd.getMessageAction().equalsIgnoreCase(QUERY_ACTION)) {
            // query functionality

            // retrieve a query spec object from AppConfig and populate it with
            // data from the incoming message
            EmailAddressValidationQuerySpecification querySpec = (EmailAddressValidationQuerySpecification) retrieveAndBuildObject(
                    "Query Data", mmd.getQueryObject(), mmd.getQueryObjectName(), (Element) mmd.getQueryObjects().get(0));

            // Use the EmailAddressValidationProvider we're configured to use to
            // perform the actual query action
            // passing the query spec object to the query action
            try {
                EmailAddressValidation eav = this.getProvider().query(querySpec);
                if (eav == null) {
                    info("null query response ... returning empty reply");

                    // remove the EmailAddressValidation element from the primed
                    // Provide-Reply document
                    // to give an empty reply
                    replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());
                } else {
                    info("Adding " + mmd.getMessageObject() + " object to the Provide-Reply document.");

                    // remove the EmailAddressValidation element from the primed
                    // Provide-Reply document
                    // before adding our output Element
                    replyDoc.getRootElement().getChild(DATA_AREA).removeChildren(mmd.getMessageObject());

                    eav.setOutputLayoutManager(eav.getOutputLayoutManager("xml"));
                    Element dataAreaContent = (Element) eav.buildOutputFromObject();

                    replyDoc.getRootElement().getChild(DATA_AREA).addContent(dataAreaContent);
                }

                // return the populated Provide-Reply document...
                String replyContents = buildReplyDocument(mmd.getControlArea(), replyDoc);
                info("replyContents: " + replyContents);
                logExecutionComplete(mmd.getMessageAction(), startTime);
                return getMessage(msg, replyContents);
            } catch (ProviderException e) {
                Document genericDoc = (Document) _genericResponseReplyDoc.clone();

                e.printStackTrace();
                errMessage = "Exception occurred querying the '" + mmd.getMessageObject() + "' object.";
                logger.fatal(LOGTAG + " - " + errMessage);
                logger.fatal("Message sent in is:\n" + getMessageBody(mmd.getInDoc()));
                List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<>();
                errors.add(buildError("application", "EAVSQUERY-1001", errMessage));
                String replyContents = buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors, e);
                logExecutionComplete(mmd.getMessageAction(), startTime);
                return getMessage(msg, replyContents);
            } catch (EnterpriseLayoutException e) {
                Document genericDoc = (Document) _genericResponseReplyDoc.clone();

                e.printStackTrace();
                errMessage = "Exception occurred creating the '" + mmd.getMessageObject()
                        + "' object(s) from the data passed back from the provider.";
                logger.fatal(LOGTAG + " - " + errMessage);
                logger.fatal("Message sent in is:\n" + getMessageBody(mmd.getInDoc()));
                List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<>();
                errors.add(buildError("application", "EAVSQUERY-1002", errMessage));
                String replyContents = buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors, e);
                logExecutionComplete(mmd.getMessageAction(), startTime);
                return getMessage(msg, replyContents);
            }
        } else {
            // error: unsupported action
            Document genericDoc = (Document) _genericResponseReplyDoc.clone();

            String errType = "application";
            String errCode = "OpenEAI-1002";
            String errDesc = "Unsupported message action: " + mmd.getMessageAction() + ". " + LOGTAG + " only supports actions: '"
                    + QUERY_ACTION + "'.";
            logger.fatal(LOGTAG + " - " + errDesc);
            logger.fatal("Message sent in is:\n" + getMessageBody(mmd.getInDoc()));
            List<org.openeai.moa.objects.resources.Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            String replyContents = buildReplyDocumentWithErrors(mmd.getControlArea(), genericDoc, errors);
            logExecutionComplete(mmd.getMessageAction(), startTime);
            return getMessage(msg, replyContents);
        }
    }

    private void info(String message) {
        logger.info(LOGTAG + " - " + message);
    }

    private void logExecutionComplete(String action, long startTime) {
        long executionTime = System.currentTimeMillis() - startTime;
        info(action + "-Request execution complete in " + executionTime + " ms.");
    }

    private EmailAddressValidationProvider getProvider() {
        return provider;
    }
    private void setProvider(EmailAddressValidationProvider provider) {
        this.provider = provider;
    }

    private EmailAddressValidationProvider initializeProvider(String providerClassName) throws InstantiationException {
        try {
            info("Getting provider class for name: " + providerClassName);
            Class providerClass = Class.forName(providerClassName);

            info("Making a new instance of " + providerClass.getName());
            EmailAddressValidationProvider providerInstance = (EmailAddressValidationProvider) providerClass.newInstance();

            info("Initializing provider: " + providerInstance.getClass().getName());
            providerInstance.init(getAppConfig());
            info("Initialization complete.");

            return providerInstance;
        } catch (ClassNotFoundException e) {
            String errMsg = "Class named " + providerClassName + "not found on the classpath.  The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " - " + errMsg);
            throw new InstantiationException(errMsg);
        } catch (IllegalAccessException e) {
            String errMsg = "An error occurred getting a class for name: " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " - " + errMsg);
            throw new InstantiationException(errMsg);
        } catch (ProviderException e) {
            String errMsg = "An error occurred initializing the provider " + providerClassName + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " - " + errMsg);
            throw new InstantiationException(errMsg);
        }
    }
}
