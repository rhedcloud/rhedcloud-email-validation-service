/*******************************************************************************
 $Source$
 $Revision: 1959 $
 *******************************************************************************/

/**********************************************************************
 This file is part of the Emory eBirt Connector created by Steve Wheat
 (swheat@emory.edu) at Emory University.

 Copyright (C) 2013 Emory University. All rights reserved.
 ***********************************************************************/

package org.rhedcloud.it.services.eavs.provider;

/**
 * An exception for user object provider errors.
 * <p>
 *
 * @author Steve Wheat (swheat@emory.edu)
 * @version 1.0  - 29 April 2013
 */
public class ProviderException extends Exception {
    /**
     * Default constructor.
     */
    public ProviderException() {
        super();
    }

    /**
     * Message constructor.
     */
    public ProviderException(String msg) {
        super(msg);
    }

    /**
     * Throwable constructor.
     */
    public ProviderException(String msg, Throwable e) {
        super(msg, e);
    }
}
