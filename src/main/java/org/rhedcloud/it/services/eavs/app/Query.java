package org.rhedcloud.it.services.eavs.app;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.jms.JMSException;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;

import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.jmsobjects.network.v1_0.ElasticIp;
import edu.emory.moa.objects.resources.v1_0.EmailAddressValidationQuerySpecification;

public class Query {
	
	public class QueryThread extends Thread {
		private AppConfig appConfig;
		private XmlEnterpriseObject querySpec;
		private RequestService p2p;

		public QueryThread(AppConfig appConfig, XmlEnterpriseObject querySpec, RequestService p2p) {
			super();
			this.appConfig = appConfig;
			this.querySpec = querySpec;
			this.p2p = p2p;
		}

		public void run() {
			EmailAddressValidation aeo = null;
			List<EmailAddressValidation> eavs = null;
			try {
				aeo = (EmailAddressValidation) appConfig.getObject("EmailAddressValidation.v1_0");
				eavs = aeo.query(querySpec, p2p);
				System.out.println(eavs.size()+" eav(s) returned.");
				System.out.println("<eavs>");
				for (EmailAddressValidation eav: eavs) {
					try {
						System.out.println(eav.toXmlString());
					} catch (XmlEnterpriseObjectException e) {
						e.printStackTrace();
					}
				}
				System.out.println("</eavs>");			
			} catch (EnterpriseConfigurationObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EnterpriseObjectQueryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		public MessageProducer getMessageProducer() {
			
			return (MessageProducer) p2p;
		}
	}

	public static void main(String[] args) throws EnterpriseConfigurationObjectException, EnterpriseObjectQueryException, XmlEnterpriseObjectException, EnterpriseFieldException, JMSException, InterruptedException {
		System.out.println("Loading AppConfig");
		AppConfig appConfig = new AppConfig(args[0],args[1]);
		System.out.println("Done loading AppConfig");
		Object p2pobject = appConfig.getObject("QueryAppP2PProducer");
		ProducerPool p2ppool = null;
		PointToPointProducer p2p = null;
		if (p2pobject instanceof ProducerPool) {
			p2ppool = (ProducerPool) p2pobject;
		} else if (p2pobject instanceof PointToPointProducer) {
			p2p = (PointToPointProducer) p2pobject;
			p2p.setRequestTimeoutInterval(30000);
		}
		EmailAddressValidationQuerySpecification querySpec =
				(EmailAddressValidationQuerySpecification) appConfig.getObject("EmailAddressValidationQuerySpecification.v1_0");
		querySpec.setValidationProvider("PositiveEmailAddressValidationProvider");
		querySpec.setEmailAddress("tom@cervenka.org");
		EmailAddressValidation aeo = (EmailAddressValidation) appConfig.getObject("EmailAddressValidation.v1_0");
		Date start = new Date();
		Vector<QueryThread> queries = new Vector<QueryThread>();
		int numQueries = 1;
		for (int i=0;i<numQueries;i++) {
			QueryThread queryThread = null;
			if (p2ppool != null) {
				PointToPointProducer p2p2 = (PointToPointProducer) p2ppool.getProducer();
				p2p2.setRequestTimeoutInterval(30000);
				queryThread = new Query().new QueryThread(appConfig,querySpec,p2p2);
			} else if (p2p != null) {
				queryThread = new Query().new QueryThread(appConfig,querySpec,p2p);
			}
			queries.add(queryThread);
		}
		
		for (int i=0;i<numQueries;i++) {
			System.out.println("Starting Query number "+i);
			queries.get(i).start();
		}
		for (int i=0;i<numQueries;i++) {
			System.out.println("Joining query "+i);

				queries.get(i).join();
				p2ppool.releaseProducer(queries.get(i).getMessageProducer());
		}
		
		
		Date end = new Date();
		System.out.println("Time -> "+(end.getTime() - start.getTime())/1000+" seconds");

		//p2p.stopProducer();
		System.exit(0); //this shouldn't be necessary but without it, jvm will never exit
	}

}
