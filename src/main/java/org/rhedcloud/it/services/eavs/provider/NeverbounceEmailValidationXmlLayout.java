package org.rhedcloud.it.services.eavs.provider;

import com.neverbounce.api.model.SingleCheckResponse;
import edu.emory.moa.jmsobjects.validation.v1_0.EmailAddressValidation;
import edu.emory.moa.objects.resources.v1_0.Property;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.layouts.EnterpriseLayoutManager;
import org.openeai.layouts.EnterpriseLayoutManagerImpl;
import org.openeai.moa.XmlEnterpriseObject;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class NeverbounceEmailValidationXmlLayout extends EnterpriseLayoutManagerImpl implements EnterpriseLayoutManager, Serializable {
    /** Empty constructor */
    public NeverbounceEmailValidationXmlLayout() {
    }

    @Override
    public void init(String layoutManagerName, Document layoutDoc) throws EnterpriseLayoutException {
        super.init(layoutManagerName, layoutDoc);
    }

    @Override
    public void buildObjectFromInput(Object inputObj, XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        SingleCheckResponse nbr = (SingleCheckResponse) inputObj;
        EmailAddressValidation eav = (EmailAddressValidation) xeo;
        try {
            eav.setValidationProvider("NeverBounce");
            if ((nbr.getAddressInfo().getNormalizedEmail() == null || nbr.getAddressInfo().getNormalizedEmail().isEmpty())) {
                eav.setEmailAddress(nbr.getAddressInfo().getOriginalEmail());
            } else {
                eav.setEmailAddress(nbr.getAddressInfo().getNormalizedEmail());
            }
            eav.setStatusCode(String.valueOf(nbr.getResult().getCode()));
            eav.setStatusDescription(nbr.getResult().getDescription());
            if (nbr.getCreditsInfo() != null) {
                addProperty(eav, "PaidCreditsUsed", String.valueOf(nbr.getCreditsInfo().getPaidCreditsUsed()));
                addProperty(eav, "PaidCreditsRemaining", String.valueOf(nbr.getCreditsInfo().getPaidCreditsRemaining()));
                addProperty(eav, "FreeCreditsUsed", String.valueOf(nbr.getCreditsInfo().getFreeCreditsUsed()));
                addProperty(eav, "FreeCreditsRemaining", String.valueOf(nbr.getCreditsInfo().getFreeCreditsRemaining()));
            }
            // make flags report in a deterministic order
            StringBuilder flags = new StringBuilder();
            nbr.getFlags().stream().sorted().forEach(f -> {
                if (flags.length() != 0)
                    flags.append(",");
                flags.append(f.name());
            });
            addProperty(eav, "Flags", flags.toString());
        } catch (EnterpriseFieldException e) {
            String errMsg = "Error populating EmailAddressValidation object from NeverBounce response.  " + "The exception is: "
                    + e.getMessage();
            throw new EnterpriseLayoutException(errMsg, e);
        }
    }

    private void addProperty(EmailAddressValidation eav, String propertyName, String propertyValue) throws EnterpriseFieldException {
        Property p = eav.newProperty();
        p.setPropertyName(propertyName);
        p.setPropertyValue(propertyValue);
        eav.addProperty(p);
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo) throws EnterpriseLayoutException {
        // see
        // https://bitbucket.org/itarch/emory-moas/src/b7b4b94c5179c4604dfc49b91308b51d4592fbca/message/releases/edu/emory/Email/EmailAddressValidation/1.0/xml/Provide-Reply.xml?at=1.0.0&fileviewer=file-view-default#Provide-Reply.xml
        EmailAddressValidation eav = (EmailAddressValidation) xeo;
        Element eavEl = new Element("EmailAddressValidation");
        eavEl.addContent(new Element("ValidationProvider").setText(eav.getValidationProvider()));
        eavEl.addContent(new Element("EmailAddress").setText(eav.getEmailAddress()));
        eavEl.addContent(new Element("StatusCode").setText(eav.getStatusCode()));
        eavEl.addContent(new Element("StatusDescription").setText(eav.getStatusDescription()));
        for (Property p : (List<Property>) eav.getProperty()) {
            Element pEl = new Element("Property");
            pEl.addContent(new Element("PropertyName").setText(p.getPropertyName()));
            pEl.addContent(new Element("PropertyValue").setText(p.getPropertyValue()));
            eavEl.addContent(pEl);
        }

        return eavEl;
    }

    @Override
    public Object buildOutputFromObject(XmlEnterpriseObject xeo, String appName) throws EnterpriseLayoutException {
        setTargetAppName(appName);
        return buildOutputFromObject(xeo);
    }
}
