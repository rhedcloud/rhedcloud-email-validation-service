package org.rhedcloud.it.services.eavs;

import com.openii.openeai.commands.OpeniiRequestCommand;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.OpenEaiObject;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.moa.XmlEnterpriseObject;

public abstract class EavsRequestCommand extends OpeniiRequestCommand implements RequestCommand {
    private static final String LOGTAG = "[EavsRequestCommand] ";
    static final Logger logger = OpenEaiObject.logger;

    private boolean _verbose = false;

    public EavsRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing " + ReleaseTag.getReleaseInfo());

        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        } catch (EnterpriseConfigurationObjectException e) {
            e.printStackTrace();
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Get the verbose property.
        String verbose = getProperties().getProperty("verbose", "false");
        setVerbose(Boolean.valueOf(verbose));
        logger.info(LOGTAG + "property verbose: " + getVerbose());

        logger.info(LOGTAG + "instantiated successfully.");
    }

    /**
     * Set a parameter to toggle verbose logging.
     * @param b the verbose parameter
     */
    private void setVerbose(boolean b) {
        _verbose = b;
    }

    /**
     * Gets the value of the verbose logging parameter.
     * @return boolean the verbose parameter
     */
    private boolean getVerbose() {
        return _verbose;
    }

    protected XmlEnterpriseObject retrieveObjectFromAppConfig(String comment, String msgObject, String msgObjectName)
            throws CommandException {

        XmlEnterpriseObject xeo;

        try {
            xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObjectName);
            logger.info("xeo retrieved with " + msgObjectName + ", enterpriseFields=" + xeo.getEnterpriseFields());
        } catch (Exception e) {
            try {
                logger.warn("Could not find object named '" + msgObjectName + "' in AppConfig, trying '" + msgObject + "'");
                xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObject);
            } catch (Exception e2) {
                // very bad error
                String msg = "Could not find an object named '" + msgObject + "' OR '" + msgObjectName
                        + "' in AppConfig.  Exception: " + e2.getMessage();
                throw new CommandException(msg, e);
            }
        }
        return xeo;
    }

    /**
     *
     */
    protected XmlEnterpriseObject retrieveAndBuildObject(String comment, String msgObject, String msgObjectName,
                                                         Element eData) throws CommandException {

        XmlEnterpriseObject xeo = retrieveObjectFromAppConfig(comment, msgObject, msgObjectName);

        // now, build the object from the xml document (DataArea)...
        try {
            xeo.buildObjectFromInput(eData);
            if (comment != null) {
                if (_verbose) {
                    logger.info(comment + " Object is: " + xeo.toXmlString());
                }
            } else {
                if (_verbose) {
                    logger.info("Object is: " + xeo.toXmlString());
                }
            }
        } catch (Exception e) {
            logger.error("xeo=" + xeo);
            logger.error("data=" + new XMLOutputter().outputString(eData));
            throw new CommandException(e.getMessage(), e);
        }
        return xeo;
    }

    /**
     * Based on the 'messageAction' associated to the request we determine which
     * 'PrimedXmlDocument' associated to the message object being operated on
     * should be returned. This is specified in the Command's deployment
     * descriptor on each message object.
     * <p>
     * For example, our command supports
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * and
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request.
     * We know based on the OpenEAI Message Protocol that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Query-Request
     * is
     * com.any-erp-vendor.Person/BasicPerson/1.0/Provide-Reply.
     * We also know that the response to a
     * com.any-erp-vendor.Person/BasicPerson/1.0/Create-Request
     * is a
     * org.openeai.CoreMessaging/1.0/Response-Reply
     * with the appropriate status, action and error information filled in.
     * <p>
     * Because of all this knowledge, we configure the message objects used by
     * this Command to have the appropriate 'PrimedXmlDocument' entries that
     * allows us to simply get the appropriate reply document from the object
     * based on the action. If the Action is 'Query' we'll be returning the
     * 'primed' Provide document associated to the object. If the action is
     * anything else, we'll be returning the 'primed' generic Response document
     * associated to the object.
     **/
    protected final Document getReplyDoc(String msgAction, String msgObject, String msgRelease) throws CommandException {

        try {
            XmlEnterpriseObject xeo = (XmlEnterpriseObject) getAppConfig().getObject(msgObject + "." + generateRelease(msgRelease));
            Document rDoc;
            if (msgAction.equalsIgnoreCase(QUERY_ACTION)) {
                rDoc = xeo.getProvideDoc();
            } else {
                rDoc = xeo.getResponseDoc();
            }

            if (rDoc == null) {
                logger.fatal("Could not find a reply document for the '" + msgObject + "-" + msgAction + "' request.");
                throw new CommandException("Could not find an appropriate reply document for the '" + msgObject + "-"
                        + msgAction + " request.");
            } else {
                if (_verbose)
                    logger.info("Found a reply document for the '" + msgObject + "-" + msgAction + "' request.");
            }
            return rDoc;
        } catch (CommandException e) {
            throw e;
        } catch (Exception e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
