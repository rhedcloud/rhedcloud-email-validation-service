package org.rhedcloud.it.services.eavs;

import com.neverbounce.api.client.NeverbounceClient;
import com.neverbounce.api.client.NeverbounceClientFactory;
import com.neverbounce.api.model.Flag;
import com.neverbounce.api.model.SingleCheckResponse;

public class NbTest {
    public static void main(String[] args) {
//        String emailAddress = "khaleboyes@surgeforward.com";
        String emailAddress = "not-a-valid-email-address@gmail.com";

        NeverbounceClient neverbounceClient = NeverbounceClientFactory.create("CredentialRedactedAndRotated");
        SingleCheckResponse nbr = neverbounceClient
                .prepareSingleCheckRequest()
                .withEmail(emailAddress)
                .withAddressInfo(true)
                .withCreditsInfo(true)
                .withTimeout(300)
                .build()
                .execute();


        StringBuilder addressInfo = new StringBuilder();
        addressInfo.append("\n\tOriginalEmail=").append(nbr.getAddressInfo().getOriginalEmail());
        addressInfo.append("\n\tNormalizedEmail=").append(nbr.getAddressInfo().getNormalizedEmail());
        System.out.println("addressInfo=" + addressInfo);

        // https://neverbounce.com/help/understanding-and-downloading-results/result-codes
        switch (nbr.getResult().getCode()) {
            case 0:
                // Verified as real address
                // safe to send: YES
                System.out.println(nbr.getResult().getDescription());
                break;
            case 1:
                // Verified as not valid
                // safe to send: NO
                System.out.println(nbr.getResult().getDescription());
                break;
            case 2:
                // A temporary, disposable address
                // safe to send: NO
                System.out.println(nbr.getResult().getDescription());
                break;
            case 3:
                // A domain-wide setting
                // safe to send: MAYBE
                System.out.println(nbr.getResult().getDescription());
                break;
            case 4:
                // The server cannot be reached
                // safe to send: NO
                System.out.println(nbr.getResult().getDescription());
                break;
        }
        StringBuilder flags = new StringBuilder();
        for (Flag f : nbr.getFlags()) {
            if (flags.length() != 0)
                flags.append(",");
            flags.append(f.name());
        }
        System.out.println("flags=" + flags);

        StringBuilder creditsInfo = new StringBuilder();
        creditsInfo.append("\n\tPaidCreditsUsed=").append(nbr.getCreditsInfo().getPaidCreditsUsed());
        creditsInfo.append("\n\tPaidCreditsRemaining=").append(nbr.getCreditsInfo().getPaidCreditsRemaining());
        creditsInfo.append("\n\tFreeCreditsUsed=").append(nbr.getCreditsInfo().getFreeCreditsUsed());
        creditsInfo.append("\n\tFreeCreditsRemaining=").append(nbr.getCreditsInfo().getFreeCreditsRemaining());
        System.out.println("creditsInfo=" + creditsInfo);
    }
}
