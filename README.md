# The RHEDcloud Email Address Validation Service

##Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

##License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

##Description

This project implements an ESB service (EAVS) that validates an email address
 and will be used in the AWS account provisioning orchestration process.

The service validates whether or not email addresses exist and if an email
 can be delivered to them.  An example of its use is during the account
 provisioning process where it is invoked to determine if the next set of
 distribution lists pre-provisioned by the Messaging Team actually exist
 and work. This service can also be used to count the distribution list
 inventory and alert the Messaging Team if the inventory of email addresses
 is running low.

The service will use the NeverBounce commercial email validation API
 but will be written in a way that allows other providers to be used instead.
 Many commercial email address validation services allow batches of email
 addresses to be validated, but NeverBounce also allows for a single address
 to be validated.  It is the single address mode that will be implemented first.
