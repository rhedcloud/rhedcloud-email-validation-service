<?xml version="1.0"?>

<!--
testing at http://xslttest.appspot.com/
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" omit-xml-declaration="no" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*"/>

    <!-- the identity template (copies your input verbatim) -->
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="PropertyValue/text()">
        <xsl:text>987654321</xsl:text>
    </xsl:template>

</xsl:stylesheet>
