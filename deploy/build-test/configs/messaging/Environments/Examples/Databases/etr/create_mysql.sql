/*     $Revision: 788 $
       $Date: 2003-03-05 18:36:31 +0000 (Wed, 05 Mar 2003) $
       $Source$
*/

DROP DATABASE IF EXISTS etr;

CREATE DATABASE etr;

USE etr;

CREATE TABLE `T_PROCESSED_MESSAGE` (
  `PROCESSED_MESSAGE_ID` varchar(255) NOT NULL default '',
  `MESSAGE_SERIES` varchar(255) default NULL,
  `CREATE_USER` varchar(30) NOT NULL default '',
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY `PK_PROCESSED_MESSAGE` (`PROCESSED_MESSAGE_ID`)
) TYPE=MyISAM; 

GRANT ALL ON etr.* TO etr@localhost IDENTIFIED BY 'etr22';
