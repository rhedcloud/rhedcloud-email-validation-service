/*		A script to generate or regenerate the
		EnterpriseApplicationService database store
		in a MySQL database.

		$Revision: 739 $
		$Date: 2003-02-24 21:04:16 +0000 (Mon, 24 Feb 2003) $
		$Source$
*/
/************************************************************/

/***************** T_PROCESSED_MESSAGE **********************/
drop table if exists T_PROCESSED_MESSAGE;
create table T_PROCESSED_MESSAGE (
  PROCESSED_MESSAGE_ID   VARCHAR(255) not null,
  MESSAGE_SERIES         VARCHAR(255) not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,             
  primary key (PROCESSED_MESSAGE_ID)
);

/************************ T_NET_ID **************************/
drop table if exists T_NET_ID;
create table T_NET_ID (
  NET_ID_ID              BIGINT AUTO_INCREMENT not null,
  PRINCIPAL              VARCHAR(50) not null,
  DOMAIN                 VARCHAR(50) not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),            
  MOD_DATE               DATETIME,
  primary key (NET_ID_ID),
  unique key (PRINCIPAL, DOMAIN)
);

/****************** T_NET_ID_ASSIGNMENT *********************/
drop table if exists T_NET_ID_ASSIGNMENT;
create table T_NET_ID_ASSIGNMENT (
  ASSIGNMENT_ID          BIGINT AUTO_INCREMENT not null,
  NET_ID_ID              BIGINT not null references T_NET_ID(NET_ID_ID),
  INST_ID                Varchar(9) not null,
  START_DATE             DATETIME not null,
  END_DATE               DATETIME,
  TERMINATED_DATE        DATETIME,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),           
  MOD_DATE               DATETIME,
  primary key (ASSIGNMENT_ID),
  unique key (NET_ID_ID, INST_ID)
);

/********************* T_NET_ID_HOLD ************************/
drop table if exists T_NET_ID_HOLD;
create table T_NET_ID_HOLD (
  HOLD_ID                BIGINT AUTO_INCREMENT not null,
  NET_ID_ID              BIGINT not null references T_NET_ID(NET_ID_ID),
  START_DATE             DATETIME not null,
  END_DATE               DATETIME,
  TERMINATED_DATE        DATETIME,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),           
  MOD_DATE               DATETIME,
  primary key (HOLD_ID)
);

/******************* T_ENTERPRISE_USER **********************/
drop table if exists T_ENTERPRISE_USER;
create table T_ENTERPRISE_USER (
  INST_ID                VARCHAR(9) not null,
  DATA_AREA              TEXT not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),            
  MOD_DATE               DATETIME,
  primary key (INST_ID)
);

/******************* T_GENERAL_PROPERTY *********************/
drop table if exists T_GENERAL_PROPERTY;
create table T_GENERAL_PROPERTY (
  PROPERTY_ID            BIGINT not null AUTO_INCREMENT,
  PROPERTY_TYPE          VARCHAR(100) not null,
  PROPERTY_NAME          VARCHAR(100) not null,
  PROPERTY_VALUE         text not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),            
  MOD_DATE               DATETIME,
  primary key (PROPERTY_ID),
  unique key (PROPERTY_TYPE, PROPERTY_NAME)
);

/****************** T_ENTERPRISE_SESSION ********************/
drop table if exists T_ENTERPRISE_SESSION;
create table T_ENTERPRISE_SESSION (
  ENTERPRISE_SESSION_ID        BIGINT not null AUTO_INCREMENT,
  ENTERPRISE_SESSION_COOKIE_ID VARCHAR(255) not null,
  NET_ID_ID                    BIGINT not null references T_NET_ID(NET_ID_ID),
  START_DATE                   DATETIME not null,
  LAST_ACTIVITY_DATE           DATETIME,
  END_DATE                     DATETIME,  
  CLIENT_IP                    VARCHAR(20) not null,
  SESSION_ENV                  TEXT,
  STATUS                       VARCHAR(255) default 'logged' not null,
  CREATE_USER                  VARCHAR(30) not null,
  CREATE_DATE                  DATETIME not null,
  MOD_USER                     VARCHAR(30),             
  MOD_DATE                     DATETIME,
  primary key (ENTERPRISE_SESSION_ID),
  unique key (ENTERPRISE_SESSION_COOKIE_ID)
);

/************** T_ENTERPRISE_SESSION_ARCHIVE ****************/
drop table if exists T_ENTERPRISE_SESSION_ARCHIVE;
create table T_ENTERPRISE_SESSION_ARCHIVE (
  ENTERPRISE_SESSION_ID        BIGINT not null AUTO_INCREMENT,
  ENTERPRISE_SESSION_COOKIE_ID VARCHAR(255) not null,
  NET_ID_ID                    BIGINT not null references T_NET_ID(NET_ID_ID),
  START_DATE                   DATETIME not null,
  LAST_ACTIVITY_DATE           DATETIME not null,
  END_DATE                     DATETIME not null,  
  CLIENT_IP                    VARCHAR(20) not null,
  SESSION_ENV                  TEXT not null,
  STATUS                       VARCHAR(255) default 'logged' not null,
  CREATE_USER                  VARCHAR(30) not null, 
  CREATE_DATE                  DATETIME not null,
  MOD_USER                     VARCHAR(30),            
  MOD_DATE                     DATETIME,
  primary key (ENTERPRISE_SESSION_ID),
  unique key (ENTERPRISE_SESSION_COOKIE_ID)
);

/********************* T_APPLICATION ************************/
drop table if exists T_APPLICATION;
create table T_APPLICATION (
  APPL_ID            BIGINT not null AUTO_INCREMENT,
  APPL_NAME          VARCHAR(255) not null,
  APPL_DESCR         TEXT not null,
  SECRET_KEY         TEXT,
  CREATE_USER        VARCHAR(30) not null,
  CREATE_DATE        DATETIME not null,
  MOD_USER           VARCHAR(30),            
  MOD_DATE           DATETIME,
  primary key (APPL_ID),
  unique key (APPL_NAME)
);

/***************** T_APPL_SESSION_PROPERTY ******************/
drop table if exists T_APPL_SESSION_PROPERTY;
create table T_APPL_SESSION_PROPERTY (
  PROPERTY_ID        BIGINT not null AUTO_INCREMENT,
  APPL_ID            BIGINT not null references T_APPLICATION(APPL_ID),
  PROPERTY_NAME      VARCHAR(255) not null,
  PROPERTY_VALUE     TEXT not null,
  CREATE_USER        VARCHAR(30) not null,
  CREATE_DATE        DATETIME not null,
  MOD_USER           VARCHAR(30),            
  MOD_DATE           DATETIME,
  primary key (PROPERTY_ID),
  unique key (APPL_ID, PROPERTY_NAME)
);

/********************* T_APPL_SESSION ***********************/
drop table if exists T_APPL_SESSION;
create table T_APPL_SESSION (
  APPL_SESSION_ID        BIGINT not null AUTO_INCREMENT,
  APPL_ID                BIGINT not null references T_APPLICATION(APPL_ID),
  APPL_SESSION_COOKIE_ID VARCHAR(255) not null,
  ENTERPRISE_SESSION_ID  BIGINT not null references T_ENTERPRISE_SESSION(ENTERPRISE_SESSION_ID),
  START_DATE             DATETIME not null,
  LAST_ACTIVITY_DATE     DATETIME,
  END_DATE               DATETIME,  
  CLIENT_IP              VARCHAR(20),
  SESSION_ENV            TEXT,
  STATUS                 VARCHAR(255) default 'logged' not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),             
  MOD_DATE               DATETIME,
  primary key (APPL_SESSION_ID),
  unique key (APPL_SESSION_COOKIE_ID)
);

/****************** T_APPL_SESSION_ARCHIVE ******************/
drop table if exists T_APPL_SESSION_ARCHIVE;
create table T_APPL_SESSION_ARCHIVE (
  APPL_SESSION_ID        BIGINT not null AUTO_INCREMENT,
  APPL_ID                BIGINT not null references T_APPLICATION(APPL_ID),
  APPL_SESSION_COOKIE_ID VARCHAR(255) not null,
  ENTERPRISE_SESSION_ID  BIGINT not null references T_ENTERPRISE_SESSION_ARCHIVE(ENTERPRISE_SESSION_ID),
  START_DATE             DATETIME not null,
  LAST_ACTIVITY_DATE     DATETIME not null,
  END_DATE               DATETIME not null,  
  CLIENT_IP              VARCHAR(20),
  SESSION_ENV            TEXT,
  STATUS                 VARCHAR(255) default 'logged' not null,
  CREATE_USER            VARCHAR(30) not null,
  CREATE_DATE            DATETIME not null,
  MOD_USER               VARCHAR(30),             
  MOD_DATE               DATETIME,
  primary key (APPL_SESSION_ID),
  unique key (APPL_SESSION_COOKIE_ID)
);

