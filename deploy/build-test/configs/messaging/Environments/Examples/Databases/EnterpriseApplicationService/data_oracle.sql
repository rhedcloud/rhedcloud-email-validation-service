/*		A script to insert test data into the
		EnterpriseApplicationService database store
		in an Oracle database.

		$Revision: 739 $
		$Date: 2003-02-24 21:04:16 +0000 (Mon, 24 Feb 2003) $
		$Source$
*/
/************************************************************/
INSERT INTO T_NET_ID(NET_ID_ID,PRINCIPAL,DOMAIN,CREATE_USER,CREATE_DATE) VALUES(1,'ziggy','any-openeai-enterprise.org','eas',sysdate);
INSERT INTO T_NET_ID_ASSIGNMENT(ASSIGNMENT_ID,NET_ID_ID,INST_ID,START_DATE,CREATE_USER,CREATE_DATE) VALUES (1,1,'123456789','11-FEB-03','eas',sysdate);
INSERT INTO T_ENTERPRISE_USER(INST_ID,DATA_AREA,CREATE_USER,CREATE_DATE) VALUES('123456789','<EnterpriseUser><NetId><Principal>ziggy</Principal><Domain>any-openeai-enterprise.org</Domain></NetId><LightweightPerson><Name><LastName>Stardust</LastName><FirstName>Ziggy</FirstName></Name><InstitutionalId>123456789</InstitutionalId></LightweightPerson></EnterpriseUser>','eas',sysdate);
INSERT INTO T_APPLICATION(APPL_ID,APPL_NAME,APPL_DESCR,SECRET_KEY,CREATE_USER,CREATE_DATE) VALUES(1,'org.any-openeai-enterprise.TestSuiteApplication','Test Suite Application','8a1e9460-be8a-4d49-a3c2-b5d2fa08ca57','eas',sysdate);
INSERT INTO T_APPLICATION(APPL_ID,APPL_NAME,APPL_DESCR,SECRET_KEY,CREATE_USER,CREATE_DATE) VALUES(2,'org.any-openeai-enterprise.EnterpriseAuthenticationServlet','Enterprise Authentication Servlet','8a1e9460-be8a-4d49-a3c2-b5d2fa08ca07','eas',sysdate);
INSERT INTO T_GENERAL_PROPERTY(PROPERTY_ID,PROPERTY_TYPE,PROPERTY_NAME,PROPERTY_VALUE,CREATE_USER,CREATE_DATE) VALUES(1,'EnterpriseSession','maxIdleTime','600','eas',sysdate);
INSERT INTO T_APPL_SESSION_PROPERTY(PROPERTY_ID,APPL_ID,PROPERTY_NAME,PROPERTY_VALUE,CREATE_USER,CREATE_DATE) VALUES(1,1,'maxIdleTime','300','eas',sysdate);
INSERT INTO T_APPL_SESSION_PROPERTY(PROPERTY_ID,APPL_ID,PROPERTY_NAME,PROPERTY_VALUE,CREATE_USER,CREATE_DATE) VALUES(2,2,'maxIdleTime','300','eas',sysdate);
COMMIT;