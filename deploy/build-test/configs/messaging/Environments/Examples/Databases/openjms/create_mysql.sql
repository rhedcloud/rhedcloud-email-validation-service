/*     $Revision: 788 $
       $Date: 2003-03-05 18:36:31 +0000 (Wed, 05 Mar 2003) $
       $Source$
*/

DROP DATABASE IF EXISTS openjms;

CREATE DATABASE openjms;
GRANT ALL ON openjms.* TO openjms@localhost IDENTIFIED BY 'openjms22';

