/*     $Revision: 793 $
       $Date: 2003-03-07 18:39:17 +0000 (Fri, 07 Mar 2003) $
       $Source$
*/

DROP DATABASE IF EXISTS ewh;

CREATE DATABASE ewh;

USE ewh;

CREATE TABLE `T_PROCESSED_MESSAGE` (
  `PROCESSED_MESSAGE_ID` varchar(255) NOT NULL default '',
  `MESSAGE_SERIES` varchar(255) default NULL,
  `CREATE_USER` varchar(30) NOT NULL default '',
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY `PK_PROCESSED_MESSAGE` (`PROCESSED_MESSAGE_ID`)
) TYPE=MyISAM; 

CREATE TABLE `T_PERSON` (
  `INST_ID` varchar(9) NOT NULL default '',
  `MESSAGE_CATEGORY` varchar(255) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(60) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(9) NOT NULL default '',
  `FIRST_NAME` varchar(60) NOT NULL default '',
  `MIDDLE_NAME` varchar(60) default NULL,
  `LAST_NAME` varchar(60) NOT NULL default '',
  `GENDER` char(1) default NULL,
  `ETHNICITY` char(2) default NULL,
  `BIRTH_DATE` date default NULL,
  `SSN` varchar(9) default NULL,
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `PK_PERSON` (`INST_ID`,`MESSAGE_CATEGORY`,`MESSAGE_OBJECT`,`MESSAGE_RELEASE`)
) TYPE=InnoDB; 

CREATE TABLE `T_ADDRESS` (
  `INST_ID` varchar(9) NOT NULL default '',
  `MESSAGE_CATEGORY` varchar(255) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(60) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(9) NOT NULL default '',
  `ADDR_TYPE` char(2) NOT NULL default '',
  `STREET1` varchar(60) NOT NULL default '',
  `STREET2` varchar(60) default NULL,
  `STREET3` varchar(60) default NULL,
  `CITY` varchar(60) default NULL,
  `STATE_CODE` char(2) default NULL,
  `ZIP_CODE` varchar(10) default NULL,
  `EFFECTIVE_DATE` date NOT NULL default '0000-00-00',
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `PK_ADDRESS` (`INST_ID`,`MESSAGE_CATEGORY`,`MESSAGE_OBJECT`,`MESSAGE_RELEASE`,`ADDR_TYPE`),
	FOREIGN KEY (`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`) REFERENCES T_PERSON(`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`)
) TYPE=InnoDB; 

CREATE TABLE `T_PHONE` (
  `INST_ID` varchar(9) NOT NULL default '',
  `MESSAGE_CATEGORY` varchar(255) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(60) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(9) NOT NULL default '',
  `PHONE_TYPE` char(2) NOT NULL default '',
  `PHONE_AREA` char(3) default NULL,
  `PHONE_NUMBER` varchar(10) NOT NULL default '',
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `PK_PHONE` (`INST_ID`,`MESSAGE_CATEGORY`,`MESSAGE_OBJECT`,`MESSAGE_RELEASE`,`PHONE_TYPE`),
	FOREIGN KEY (`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`) REFERENCES T_PERSON(`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`)
) TYPE=InnoDB; 

CREATE TABLE `T_EMAIL` (
  `INST_ID` varchar(9) NOT NULL default '',
  `MESSAGE_CATEGORY` varchar(255) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(60) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(9) NOT NULL default '',
  `EMAIL_TYPE` char(2) NOT NULL default '',
  `EMAIL_ADDR` varchar(60) NOT NULL default '',
  `STATUS` char(1) NOT NULL default '',
  `PREFERRED` char(1) default NULL,
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `PK_EMAIL` (`INST_ID`,`MESSAGE_CATEGORY`,`MESSAGE_OBJECT`,`MESSAGE_RELEASE`,`EMAIL_TYPE`,`STATUS`),
	FOREIGN KEY (`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`) REFERENCES T_PERSON(`INST_ID`, `MESSAGE_CATEGORY`, `MESSAGE_OBJECT`, `MESSAGE_RELEASE`)
) TYPE=InnoDB; 

GRANT ALL ON ewh.* TO ewh@localhost IDENTIFIED BY 'ewh22';
