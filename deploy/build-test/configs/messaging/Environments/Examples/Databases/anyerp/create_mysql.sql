/*     $Revision: 770 $
       $Date: 2003-02-28 19:35:31 +0000 (Fri, 28 Feb 2003) $
       $Source$
*/

DROP DATABASE IF EXISTS anyerp;

CREATE DATABASE anyerp;

USE anyerp;

CREATE TABLE `T_ERP_DATA` (
  `INST_ID` varchar(9) NOT NULL default '',
  `MESSAGE_CATEGORY` varchar(255) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(60) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(9) NOT NULL default '',
  `OBJECT_ID` int(11) NOT NULL default '1',
  `DATA_AREA` text NOT NULL,
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default '0000-00-00 00:00:00',
  `MOD_USER` varchar(30) default '',
  UNIQUE KEY `PK_ERP_DATA` (`INST_ID`,`MESSAGE_CATEGORY`,`MESSAGE_OBJECT`,`MESSAGE_RELEASE`,`OBJECT_ID`)
) TYPE=MyISAM; 

GRANT ALL ON anyerp.* TO anyerp@localhost IDENTIFIED BY 'anyerp22';
