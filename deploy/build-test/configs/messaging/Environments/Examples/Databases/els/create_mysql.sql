/*     $Revision: 759 $
       $Date: 2003-02-27 19:40:04 +0000 (Thu, 27 Feb 2003) $
       $Source$
*/

DROP DATABASE IF EXISTS els;

CREATE DATABASE els;

USE els;

CREATE TABLE `T_PROCESSED_MESSAGE` (
  `PROCESSED_MESSAGE_ID` varchar(255) NOT NULL default '',
  `MESSAGE_SERIES` varchar(255) default NULL,
  `CREATE_USER` varchar(30) NOT NULL default '',
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY `pk_processed_message` (`PROCESSED_MESSAGE_ID`)
) TYPE=InnoDB; 

CREATE TABLE `T_SYNC_ERROR_MSG` (
  `SYNC_ERROR_MSG_ID` bigint(11) NOT NULL auto_increment,
  `CONSUMING_APPL_NAME` varchar(50) NOT NULL default '',
  `CONSUMING_PRODUCER_ID` varchar(50) NOT NULL default '',
  `CONSUMING_MSG_SEQ` varchar(10) NOT NULL default '',
  `PRODUCING_APPL_NAME` varchar(50) NOT NULL default '',
  `PRODUCING_PRODUCER_ID` varchar(50) NOT NULL default '',
  `PRODUCING_MSG_SEQ` varchar(10) NOT NULL default '',
  `PRODUCING_MSG_ACTION` varchar(20) NOT NULL default '',
  `MESSAGE` text NOT NULL,
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `PK_SYNC_ERROR_MSG` (`SYNC_ERROR_MSG_ID`),
  UNIQUE KEY `UK_SYNC_ERROR_MSG_1` (`CONSUMING_APPL_NAME`,`CONSUMING_PRODUCER_ID`,`CONSUMING_MSG_SEQ`)
) TYPE=InnoDB; 

CREATE TABLE `T_SYNC_ERROR` (
  `SYNC_ERROR_ID` bigint(11) NOT NULL auto_increment,
  `SYNC_ERROR_MSG_ID` bigint(11) NOT NULL default '0',
  `ERROR_TYPE` varchar(20) NOT NULL default '',
  `ERROR_NUMBER` varchar(50) NOT NULL default '',
  `ERROR_DESC` text NOT NULL,
  `CREATE_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `CREATE_USER` varchar(30) NOT NULL default '',
  `MOD_DATE` datetime default NULL,
  `MOD_USER` varchar(30) default NULL,
  UNIQUE KEY `UK_SYNC_ERROR_1` (`SYNC_ERROR_MSG_ID`,`SYNC_ERROR_ID`),
  UNIQUE KEY `PK_SYNC_ERROR` (`SYNC_ERROR_ID`), 
	FOREIGN KEY (`SYNC_ERROR_MSG_ID`) REFERENCES T_SYNC_ERROR_MSG(`SYNC_ERROR_MSG_ID`)
) TYPE=InnoDB; 

CREATE TABLE `T_LOGGED_MESSAGE` (
  `LOGGED_MESSAGE_ID` bigint(20) NOT NULL auto_increment,
  `LOGGED_MESSAGE_DATETIME` datetime NOT NULL default '0000-00-00 00:00:00',
  `MESSAGE_CATEGORY` varchar(100) NOT NULL default '',
  `MESSAGE_OBJECT` varchar(100) NOT NULL default '',
  `MESSAGE_ACTION` varchar(20) NOT NULL default '',
  `MESSAGE_TYPE` varchar(10) NOT NULL default '',
  `MESSAGE_RELEASE` varchar(5) NOT NULL default '',
  `MESSAGE_PRIORITY` int(11) NOT NULL default '0',
  `MESSAGE` text NOT NULL,
  `SENDER_APPL_ID` varchar(255) NOT NULL default '',
  `PRODUCER_ID` varchar(255) NOT NULL default '',
  `MESSAGE_SEQ` bigint(10) NOT NULL default '0',
  `MESSAGE_DATETIME` datetime NOT NULL default '0000-00-00 00:00:00',
  `LOG_CONSUMER_ID` varchar(255) NOT NULL default '',
  `LOG_DB_USER` varchar(10) NOT NULL default '',
  `TARGET_APP_NAME` varchar(255) default NULL,
  UNIQUE KEY `PK_LOGGED_MESSAGE` (`LOGGED_MESSAGE_ID`)
) TYPE=MyISAM; 

GRANT ALL ON els.* TO els@localhost IDENTIFIED BY 'els22';
