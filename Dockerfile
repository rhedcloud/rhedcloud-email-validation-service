FROM emory/esb-service-base-1.0
MAINTAINER Steve Wheat <swheat@emory.edu>

# copy the environment deployment artifacts to the image
COPY deploy /opt/openeai/deploy

# copy the newly build code to the deployment directory
COPY target/*.jar /opt/openeai/deploy/build-test/libs/EmailAddressValidationService

# Run the service when the container runs
ENTRYPOINT [/opt/openeai/deploy/build-test/bin/start.sh EmailAddressValidationService]
